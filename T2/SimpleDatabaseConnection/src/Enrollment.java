import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Enrollment {

	public static String enrollStudent(int studentId, int courseId){
		String result = "";
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/database", "root", "password");
			String query = "INSERT INTO enrolment (studentId, courseId)"+
						"VALUES ('"+studentId+"','"+courseId+"'); ";
			
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(query);
			query = "SELECT * FROM enrolment";
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {				
				result += rs.getString("studentId")+rs.getString("courseId");
				
			}
			conn.close();			
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return e.getMessage();
		}
	}
	public static String unenrollStudent(int studentId, int courseId){
		String result="";
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/database", "root", "password");
			String query = "DELETE FROM enrolment"+
						" WHERE studentId='"+studentId+"' AND courseId='"+courseId+"'; ";
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(query);
			query = "SELECT * FROM enrolment";
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);			
			while (rs.next()) {				
				result += rs.getString("studentId")+rs.getString("courseId");
				
			}
			conn.close();			
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return e.getMessage();
		}
	}
public static String getAll(){
		
		String result = "";
		
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/database", "root", "password");
			String query = "SELECT * FROM enrolment;";
			
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				result += rs.getString("studentId")+rs.getString("courseId");				
			}
			conn.close();
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return e.toString();
		}
	}
}
