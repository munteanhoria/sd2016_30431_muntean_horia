import static org.junit.Assert.*;

import org.junit.Test;

public class CourseTest {

	@Test
	public void testInsertCourse1() {
		String testStr = Course.insertCourse(10, "Course1","Teacher1",1);
		String expResult = Course.getAllCourse();
	    assertEquals(expResult,testStr);
	}
	
	@Test
	public void testInsertCourse2() {
		String testStr = Course.insertCourse(20, "Course2","Teacher2",2);
		String expResult = Course.getAllCourse();
	    assertEquals(expResult,testStr);
	}

	@Test
	public void testUpdateStudent1() {
		String testStr = Course.updateCourse(10, "Course10","Teacher10",2);
		String expResult = Course.getAllCourse();
	    assertEquals(expResult,testStr);
	}
	
	@Test
	public void testUpdateStudent2() {
		String testStr = Course.updateCourse(20, "Course20","Teacher20",2);
		String expResult = Course.getAllCourse();
	    assertEquals(expResult,testStr);
	}

	@Test
	public void testDeleteStudent1() {
		String testStr = Course.deleteCourse(10);
		String expResult = Course.getAllCourse();
	    assertEquals(expResult,testStr);		
	}
	@Test
	public void testDeleteStudent2() {
		String testStr = Course.deleteCourse(20);
		String expResult = Course.getAllCourse();
	    assertEquals(expResult,testStr);			
	}

}
