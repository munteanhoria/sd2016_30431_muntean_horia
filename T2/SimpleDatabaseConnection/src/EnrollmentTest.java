import static org.junit.Assert.*;

import org.junit.Test;

public class EnrollmentTest {

	@Test
	public void testEnrollStudent1() {
		String testStr = Enrollment.enrollStudent(100,100);
		String expResult = Enrollment.getAll();
	    assertEquals(expResult,testStr);
	}
	
	@Test
	public void testEnrollStudent2() {
		String testStr = Enrollment.enrollStudent(200,200);
		String expResult = Enrollment.getAll();
	    assertEquals(expResult,testStr);
	}

	@Test
	public void testUnenrollStudent1() {
		String testStr = Enrollment.unenrollStudent(100,100);
		String expResult = Enrollment.getAll();
	    assertEquals(expResult,testStr);
	}
	
	@Test
	public void testUnenrollStudent2() {
		String testStr = Enrollment.unenrollStudent(200,200);
		String expResult = Enrollment.getAll();
	    assertEquals(expResult,testStr);
	}

}
