import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Student {

	public static String insertStudent(int id, String firstName, String lastName, Date birthDate, String address){
		String result = "";
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/database", "root", "password");
			String query = "INSERT INTO student (idStudent, FirstName, LastName, BirthDate, Address)"+
						"VALUES ('"+id+"','"+firstName+"','"+lastName+"','"+birthDate+"','"+address+"'); ";
			
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(query);
			
			
			query = "SELECT * FROM student WHERE idStudent="+id+";";
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				System.out.println("Student: " + rs.getString("idStudent"));
				System.out.println("Student: " + rs.getString("FirstName"));
				System.out.println("Student: " + rs.getString("LastName"));
				System.out.println("Student: " + rs.getString("BirthDate"));
				System.out.println("Student: " + rs.getString("Address"));
				
				result += rs.getString("idStudent")+rs.getString("FirstName")+ rs.getString("LastName")+ rs.getString("BirthDate")+ rs.getString("Address");
				
			}
			conn.close();
			return result;			
		} catch (Exception e) {
			System.out.println(e);
			return e.toString();
		}
	}
	
	public static String updateStudent(int id, String firstName, String lastName, Date birthDate, String address){
			String result = ""; 
		
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/database", "root", "password");
			String query = "UPDATE student "+
					"SET FirstName='"+firstName+"', LastName='"+lastName+"', BirthDate='"+birthDate+"', Address='"+address+
					"' WHERE idStudent ="+id+";";			
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(query);
			query = "SELECT * FROM student WHERE idStudent="+id+";";
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {				
				result += rs.getString("idStudent")+rs.getString("FirstName")+ rs.getString("LastName")+ rs.getString("BirthDate")+ rs.getString("Address");
				
			}
			conn.close();			
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Student "+e);
			return e.toString();
		}
	}
	
	public static String deleteStudent(int id){
		String result = "";
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/database", "root", "password");
			String query = "DELETE FROM student "+
						"WHERE idStudent="+id+"; ";
			
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(query);
			query = "SELECT * FROM student WHERE idStudent="+id+";";
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {				
				result += rs.getString("idStudent")+rs.getString("FirstName")+ rs.getString("LastName")+ rs.getString("BirthDate")+ rs.getString("Address");
				
			}
			conn.close();
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return e.toString();
		}
	}
	
	public static String getAllStudents(){
		
		String result = "";
		
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/database", "root", "password");
			String query = "SELECT * FROM student;";
			
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				
				result += rs.getString("idStudent")+rs.getString("FirstName")+ rs.getString("LastName")+ rs.getString("BirthDate")+ rs.getString("Address");
				
			}
			conn.close();
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return e.toString();
		}
	}
	
public static String getStudent(int id){
		
		String result = "";
		
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/database", "root", "password");
			String query = "SELECT * FROM student WHERE idStudent="+id+";";
			
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				
				result += rs.getString("idStudent")+rs.getString("FirstName")+ rs.getString("LastName")+ rs.getString("BirthDate")+ rs.getString("Address");
				
			}
			conn.close();
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return e.toString();
		}
	}
}
