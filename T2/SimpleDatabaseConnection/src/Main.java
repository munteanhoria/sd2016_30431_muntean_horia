import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import java.sql.*;

public class Main {

	public static void main(String[] args) {
		
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/database", "root", "password");
			String query = "SELECT * FROM student";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				System.out.println("Student: " + rs.getString("idStudent"));
				System.out.println("Student: " + rs.getString("FirstName"));
				System.out.println("Student: " + rs.getString("LastName"));
				System.out.println("Student: " + rs.getString("BirthDate"));
				System.out.println("Student: " + rs.getString("Address"));
			}
			
			query = "SELECT * FROM course";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				System.out.println("Course: " + rs.getString("idCourse"));
				System.out.println("Course: " + rs.getString("CourseName"));
				System.out.println("Course: " + rs.getString("Teacher"));
				System.out.println("Course: " + rs.getString("StudyYear"));
			}
			
			query = "SELECT * FROM enrolment";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				System.out.println("Enrollment: " + rs.getString("studentId"));
				System.out.println("Enrollment: " + rs.getString("courseId"));
			}
		} catch (Exception e) {
			System.err.println(e);
		}
		
		
		Result result = JUnitCore.runClasses(StudentTest.class,CourseTest.class,EnrollmentTest.class);
	      for (Failure failure : result.getFailures()) {
	         System.out.println(failure.toString());
	      }
	      System.out.println(result.wasSuccessful());

	}

}
