import static org.junit.Assert.*;
import java.sql.Date;
import org.junit.Test;

public class StudentTest {

	@Test
	public void testInsertStudent1() {
		String testStr = Student.insertStudent(20, "First2", "Last2", new Date(1994,2,2), "addr2");
		String expResult = Student.getStudent(20);
	    assertEquals(expResult,testStr);
	}
	
	@Test
	public void testInsertStudent2() {
		String testStr = Student.insertStudent(30, "First3", "Last3", new Date(1994,3,3), "addr3");
		String expResult = Student.getStudent(30);
	    assertEquals(expResult,testStr);
	}

	@Test
	public void testUpdateStudent1() {
		String testStr = Student.updateStudent(20, "First22", "Last22", new Date(1994,2,2), "addr22");
		String expResult = Student.getStudent(20);
	    assertEquals(expResult,testStr);
	}
	
	@Test
	public void testUpdateStudent2() {
		String testStr = Student.updateStudent(30, "First33", "Last33", new Date(1994,3,3), "addr33");
		String expResult = Student.getStudent(30);
	    assertEquals(expResult,testStr);
	}

	@Test
	public void testDeleteStudent1() {
		Student.deleteStudent(20);
		String expResult = Student.getStudent(20);
	    assertEquals(expResult,"");		
	}
	@Test
	public void testDeleteStudent2() {
		Student.deleteStudent(30);
		String expResult = Student.getStudent(30);
	    assertEquals(expResult,"");		
	}

}
