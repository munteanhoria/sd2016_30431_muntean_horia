import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Course {
	public static String insertCourse(int id, String name, String teacher, int studyYear){
		String result = "";
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/database", "root", "password");
			String query = "INSERT INTO course (idCourse, CourseName, Teacher, StudyYear)"+
						"VALUES ('"+id+"','"+name+"','"+teacher+"','"+studyYear+"'); ";
			
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(query);
			query = "SELECT * FROM course";
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {				
				result += rs.getString("idCourse")+rs.getString("CourseName")+ rs.getString("Teacher")+ rs.getString("StudyYear");
				
			}
			conn.close();			
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			return e.getMessage();
		}
	}
	
	public static String updateCourse(int id, String name, String teacher, int studyYear){
		String result ="";
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/database", "root", "password");
			String query = "UPDATE course "+
					"SET CourseName='"+name+"', Teacher='"+teacher+"', StudyYear='"+studyYear+
					"' WHERE idCourse ="+id+";";
			
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(query);
			query = "SELECT * FROM course";
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {				
				result += rs.getString("idCourse")+rs.getString("CourseName")+ rs.getString("Teacher")+ rs.getString("StudyYear");
				
			}
			conn.close();			
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return e.getMessage();
			
		}
	}
	
	public static String deleteCourse(int id){
		String result ="";
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/database", "root", "password");
			String query = "DELETE FROM course "+
					"WHERE idCourse="+id+"; ";
			
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(query);
			query = "SELECT * FROM course";
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {				
				result += rs.getString("idCourse")+rs.getString("CourseName")+ rs.getString("Teacher")+ rs.getString("StudyYear");
				
			}
			conn.close();			
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return e.getMessage();
		}
	}
public static String getAllCourse(){
		
		String result = "";
		
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/database", "root", "password");
			String query = "SELECT * FROM course;";
			
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				result += rs.getString("idCourse")+rs.getString("CourseName")+ rs.getString("Teacher")+ rs.getString("StudyYear");				
			}
			conn.close();
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return e.toString();
		}
	}
}
